---
layout: program
title: Flashback
image: flashback.jpg
type: Urban Mix
---

Emission de découverte ou re-découverte des tubes, qui sont la base des morceaux actuels, le funk et disco des années 80 à 90 incontournable sur les pistes de discothèques
