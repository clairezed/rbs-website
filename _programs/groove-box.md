---
layout: program
title: Groove Box
image: groovebox.jpg
type: Clubbing
---

En solo ou en duo d’un vendredi à l’autre, avec ou sans 'Guest', le style musical des trois résidents de l’émission 'Groovebox' est 100% électronique et varie en fonction de l'humeur et des nouveautés entre House, Minimal et Progressive
