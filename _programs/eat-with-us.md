---
layout: program
title: Eat with us
type: Urban Mix
image: eatwithus.jpg
---

Eath with us est un restaurant où l’on sert de la musique : les cuistots sont des dj’s, les ingrédients des morceaux de musique et les fourneaux des platines, la musique servie est variée, colorée, les recettes s’inspirent des manuels d’antan et des nouvelles tendances pour amener cette saveur unique, le cadre est charmant : la liberté de mixer règne au sein d’RBS, l’énergie de l’équipe et le service est impeccable : la marque de l’école TURNTABLEAST.
