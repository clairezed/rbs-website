---
layout: program
title: Afrique Hebdo
image: afriquehebdo.jpg
type: Cultures du monde
schedule: Le mardi de 10h à 11h
period: De janvier à juin
presenter: ACNI
live: true
website_url: http://www.facebook.com/afriq.hebdo
rss_url: http://www8.vps.radiorbs.com/podcasts/playlists//RSS/RSS_Afrique-Hebdo.xml
---

Magazine consacré à l'actualité des pays Africains et Caraïbéens. Retrouvez les informations politiques, économiques, culturelles et sportives de l'Afrique Noire, le tout entre-mêlé des musiques africaine, antillaise, ...
