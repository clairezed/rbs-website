---
layout: program
title: Cite Cultures
type: Talk
image: /citecultures.png
---

Cité Cultures est votre rendez-vous culturel hebdomadaire sur RBS. Chaque semaine, un ou deux invités, acteur ou grand témoin de la vie culturelle strasbourgeoise vient parler de son actualité, ou évoquer un sujet qui lui tient à coeur. L’équipe des chroniqueurs de Cité Cultures est là pour vous divertir et vous surprendre.
