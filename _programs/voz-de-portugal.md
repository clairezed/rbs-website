---
layout: program
title: Voz-de-Portugal
image: vozdeportugal.jpg
type: Cultures du Monde
---

Emission des résidants portugais de Strasbourg avec un théme d'actualité. Musiques portugaises tous styles, informations nationales et régionales, et evénements de l'ACPS et autres association.
