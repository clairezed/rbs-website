---
layout: program
title: Tapaze
image: tapaze.jpg
type: Cultures du monde
---

Retrouvez Irwin et toute l’équipe Tapaze pour faire le plein de soleil : musiques des îles de l'océan indien, sega, maloya, musique tropicales, dédicaces et pleins de surprises !
