---
layout: program
title: No limit
image: nolimit.jpg
type: Urban Mix
---

Voici le nouveau duo explosif de la scène clubing Strasbourgeoise

Artemis Gordon est le Mc ambianceur le plus dynamique et atypique de l'Est de la France. Il est l’ancien Mc officiel du Kissclub durant 4 années qui tourne actuellement dans la France entière pour faire vibrer les dancefloors. Il est accompagné de Dj Timal, l'un des Dj les plus talentueux de sa génération et grand gagnant du concours Move en Scène 2011 du meilleur Dj d'Alsace !
