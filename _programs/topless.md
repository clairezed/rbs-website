---
layout: program
title: Topless
image: topless.jpg
type: Talk
---

Topless est une émission qui revendique un hip hop américain authentique, old school ou récent, parfois sous estimé, ou parfois méconnu du grand public. Le tout sans prise de tête et dans la bonne humeur.
