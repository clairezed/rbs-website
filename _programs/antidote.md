---
layout: program
title: Antidote
image: antidote.jpg
type: Clubbing
---

Voilà près de 10 ans que DJ Cut EDge enchaine les mixes sur ses MK2 ... La musique, pour lui, est une question de partage, une vocation même !
