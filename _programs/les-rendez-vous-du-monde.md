---
layout: program
title: Les Rendez vous du Monde
image: lesrendezvousdumonde.jpg
type: Cultures du monde
---

Magazine hebdomadaire sur les communautés strasbourgeoises, sur la vie associative, avec des interviews de personnalités ou artistes.
