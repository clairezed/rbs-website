---
layout: program
title: Mojito FC
type: Talk
image: mojitofc.png

---

En deux mots, le Mojito FC c’est à la fois : des débats de fond autour de l’actu foot internationale, des rubriques de décryptage sur des thématiques originales, des séquences mag’ consacrées à l’histoire du football, des entretiens exclusifs avec des invités présents en studio et des moments de détente, d’humour et de divertissement
