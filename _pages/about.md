---
layout: page
title: A propos
permalink: /a-propos/
---

Radio Bienvenue Strasbourg (RBS) est une radio émettant depuis la fin des années 1970 sur Strasbourg et les communes environnantes. Depuis plus de trente ans, notre station s’est construite sur deux axes forts.

## La place accordée aux « sans voix »

Depuis la création de la radio, et aujourd’hui encore, une large place est accordée aux communautés étrangères sur nos ondes. Chaque dimanche, Espagnols, Maghrébins, Portugais, Turcs, Laosien, Azéries, Mexicains, Antillais, Africains … assurent un rendez-vous incontournable et unique visant à informer nos auditeurs étrangers.
Nos programmes accordent aussi une grande importance aux artistes locaux, aux acteurs des quartiers, à travers des émissions spéciales réalisées en direct depuis ces quartiers, aux mouvements militants, et plus largement à tous ceux qui sont exclus des médias traditionnels et du débat public.

## Une ligne musicale ouverte et exigeante

Cette culture de l’ouverture et de la diversité se prolonge dans notre ligne musicale. L’axe de Radio Bienvenue Strasbourg est triple : les musiques dites «black»(hip-hop,R’n’B,soul, funk, …), les musiques électroniques (house, techno, transe, …) et les musiques racines (world, musiques du Monde, …) représentent la majeure partie du temps d’antenne.

C’est ainsi que, depuis plus de 30 ans, notre station a réussi à captiver un public de fidèles qui se reconnaît dans un format de radio loin des standards commerciaux, à la programmation musicale éclectique, et jouant avec volonté et originalité la carte de la proximité.
