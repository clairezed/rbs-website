---
layout: page
title: Mentions légales
permalink: /mentions-legales
---

Le présent site est la propriété de RBS, association loi 1901, située XXX, 67000 STRASBOURG, France, tel, radiorbsofficiel@gmail.com.

Le directeur de la publication du site Internet est XXX, président de RBS.

- Développement web : [Claire Zuliani](http://www.clairezuliani.com/)
- Design basé sur [HTML5 UP](http://html5up.net)
- Icônes : [Font awesome](http://fontawesome.io)
- Hébergement : [Netlify](https://www.netlify.com/)
